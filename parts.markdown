---
layout: page
title: Other
permalink: /parts/
---

On this page, you will see our participants.

{% for part in site.parts %}

# [{{ part.name }}: {{ part.description}}]({{ site.baseurl}}{{ part.url }})

{{ lab.summary }}

{% endfor %}

